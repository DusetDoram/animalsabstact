/**
 * Created by DuaetDoram on 21.10.2014.
 */
public abstract class animals {
    public static final String OUTPUT_FORMAT_LINE
            ="%s говорит '%s'.";

    private String name;
    private String voice;
    protected animals (String name, String voice){
        this.name=name;
        this.voice=voice;
    }
    public String getName(){return name; }

    public String getVoice(){return voice; }

    public void printDisplay() {
        System.out.println(String.format(
                OUTPUT_FORMAT_LINE,
                name,
                voice
        ));
    }
}



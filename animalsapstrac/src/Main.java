/**
 * Created by DuaetDoram on 23.10.2014.
 */
public class Main {
    public static void main(String name, String... args) {
        Dog dog = new Dog();

        Dog dog1 = new Dog("Собака", "гав-гав");

        Dog dog2 = new Dog("Тузик", "гаф-гаф");

        dog.printDisplay();
        dog1.printDisplay();
        dog2.printDisplay();

        Cat cat = new Cat();

        Cat cat1 = new Cat("Кошка", "Мяу-Мяу");

        Cat cat2 = new Cat("Тузик", "Ми-ми");

        cat.printDisplay();
        cat1.printDisplay();
        cat2.printDisplay();

        Chiakn chiakn = new Chiakn();

        Chiakn chiakn1 = new Chiakn("Цепленок", "чи-чи");

        Chiakn chiakn2 = new Chiakn("Ципа", "чиии-чиии");

        chiakn.printDisplay();
        chiakn1.printDisplay();
        chiakn2.printDisplay();

    }
}




